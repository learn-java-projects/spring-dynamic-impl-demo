# spring-dynamic-impl-demo


1) Vehicle Service is an interface that holds the all possible operation a vehicle could perform.
2) The vehicle service can be implemented by any implementation class to define the behavior.
3) That said, we could potentially have many implementation.
4) But the challenge is the request that we receive could be for any of it. So at runtime, we need to determine to invoke the corresponding implementation based on the input data.
5) Also, there could be some common service which could be shared across by any vehicle implementation (say for example, stop — just cuts the engine); such implementation can be placed in a base class and all the implementation could extend it to get the common implementation or can decide to override it too if required.
