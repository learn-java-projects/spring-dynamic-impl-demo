package com.learnjava.dynamic.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
class DynaimplApplicationTests {

	@Test
	void startHondaCar() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
	    final String baseUrl = "http://localhost:8080/start";
	    URI uri = new URI(baseUrl);
	    InputData inputData = new InputData();
		inputData.carMake = EnumCarMake.HONDA;
	    ResponseEntity<String> result = restTemplate.postForEntity(uri, inputData, String.class);
	}

	@Test
	void startToyotaCar() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
	    final String baseUrl = "http://localhost:8080/start";
	    URI uri = new URI(baseUrl);
	    InputData inputData = new InputData();
		inputData.carMake = EnumCarMake.TOYOTA;
	    ResponseEntity<String> result = restTemplate.postForEntity(uri, inputData, String.class);
	}
	
	@Test
	void stopHondaCar() throws URISyntaxException {
		RestTemplate restTemplate = new RestTemplate();
	    final String baseUrl = "http://localhost:8080/stop";
	    URI uri = new URI(baseUrl);
	    InputData inputData = new InputData();
		inputData.carMake = EnumCarMake.HONDA;
	    ResponseEntity<String> result = restTemplate.postForEntity(uri, inputData, String.class);
	}
}
