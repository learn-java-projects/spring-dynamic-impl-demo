package com.learnjava.dynamic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class.
 * 
 * @author MuthukumaranN
 *
 */
@RestController
public class AppController{
	
	@Autowired
	@Qualifier("vehicleService")
	VehicleService vehicleService;
	
	@PostMapping("/start")
	public void start(@RequestBody InputData inputData) {
		vehicleService.start(inputData);
	}
	
	@PostMapping("/stop")
	public void stop(@RequestBody InputData inputData) {
		vehicleService.stop(inputData);
	}
	
}
