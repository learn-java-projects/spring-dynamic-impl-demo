package com.learnjava.dynamic.impl;

public class InputData {
	
	EnumCarMake carMake;
	Object data;
	
	public EnumCarMake getCarMake() {
		return carMake;
	}
	public void setCarMake(EnumCarMake carMake) {
		this.carMake = carMake;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

}
