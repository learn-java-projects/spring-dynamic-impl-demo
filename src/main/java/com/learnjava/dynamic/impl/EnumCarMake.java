package com.learnjava.dynamic.impl;

/**
 * Car make enum.
 * 
 * @author MuthukumaranN
 *
 */
public enum EnumCarMake {
	
	HONDA, TOYOTA;

}
