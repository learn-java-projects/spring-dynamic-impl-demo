package com.learnjava.dynamic.impl;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

/**
 * Implementation for honda sedan.
 * 
 * @author MuthukumaranN
 *
 */
@Service("hondaVehicleService")
public class HondaSedan extends AbstractVehicleService implements VehicleService{
	
	@Override
	public void start(InputData inputData) {
		System.out.println("Starting Honda Sedan...");
	}
	
	@PostConstruct
	private void register() {
		VehicleServiceImpl.instances.put(EnumCarMake.HONDA, this);
	}

}
