package com.learnjava.dynamic.impl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author MuthukumaranN
 *
 */
@SpringBootApplication
public class DynaimplApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynaimplApplication.class, args);
	}

}
