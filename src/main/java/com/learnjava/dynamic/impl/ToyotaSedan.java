package com.learnjava.dynamic.impl;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

/**
 * Implementation for Toyota sedan.
 * 
 * @author MuthukumaranN
 *
 */
@Service("toyotaVehicleService")
public class ToyotaSedan extends AbstractVehicleService implements VehicleService{

	@Override
	public void start(InputData inputData) {
		System.out.println("Starting Toyota Sedan...");
	}
	
	@PostConstruct
	private void register() {
		VehicleServiceImpl.instances.put(EnumCarMake.TOYOTA, this);
	}

}
