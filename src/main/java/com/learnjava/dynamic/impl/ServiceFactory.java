package com.learnjava.dynamic.impl;

import java.util.HashMap;
import java.util.Map;

/**
 * Service Factory holds the various implementations of the service to have them
 * register and supplies the same by the type at runtime...
 * 
 * @author MuthukumaranN
 *
 * @param <K> hold the identifier/type.
 * @param <V> hold the service.
 */
public class ServiceFactory<K, V> {
	
	/**
	 * Hold the instances by type.
	 */
	public static Map<Object, Object> instances = new HashMap<>();

	/**
	 * Returns the implementation by type.
	 * 
	 * @param k
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public V getInstance(K k) {
		return (V) instances.get(k);
	}

}
