package com.learnjava.dynamic.impl;

/**
 * Vehicle Service has operations related to vehicle. 
 * 
 * @author MuthukumaranN
 *
 */
public interface VehicleService {
	
	public void start(InputData inputData);
	
	public void stop(InputData inputData);

}
