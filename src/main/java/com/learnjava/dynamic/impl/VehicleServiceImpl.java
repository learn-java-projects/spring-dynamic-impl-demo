package com.learnjava.dynamic.impl;

import org.springframework.stereotype.Service;

/**
 * Vehicle Service Implementation extends Service Factory and acts as abstraction to vehicle implementations. 
 * 
 * @author MuthukumaranN
 *
 */
@Service("vehicleService")
public class VehicleServiceImpl extends ServiceFactory<EnumCarMake, VehicleService> implements VehicleService{

	@Override
	public void start(InputData inputData) {
		getInstance(inputData.carMake).start(inputData);
	}

	@Override
	public void stop(InputData inputData) {
		getInstance(inputData.carMake).stop(inputData);
	}

}
