package com.learnjava.dynamic.impl;

/**
 * Abstract Service to hold all common implementation.
 * 
 * @author MuthukumaranN
 *
 */
public abstract class AbstractVehicleService {
	
	public void stop(InputData inputData) {
		System.out.println("Stopping the Honda/Toyota vehicle");
	}

}
